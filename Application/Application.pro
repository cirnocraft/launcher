#-------------------------------------------------
#
# Project created by QtCreator 2016-11-26T15:33:42
#
#-------------------------------------------------

QT       += core gui network widgets

TARGET = cirnocraft
TEMPLATE = app

BASE_URL = http://cirnocraft.stor.bakatrouble.pw
AUTH_BASE = https://cirnocraft.ru
# AUTH_BASE = http://127.0.0.1:8000

INCLUDEPATH += ./Sources
DEFINES += QUAZIP_STATIC \
           MANIFESTS_URL=\\\"$${BASE_URL}/manifests.json\\\" \
           AUTHENTICATE_URL=\\\"$${AUTH_BASE}/api/authenticate\\\" \
           VALIDATE_URL=\\\"$${AUTH_BASE}/api/validate\\\" \
           DEBUG_VERSION

include(../Libraries/quazip/quazip.pri)

unix {
    LIBS += -L../Libraries/quazip -lz
}

SOURCES += Sources/main.cpp \
    Sources/widgets/slidewidget.cpp \
    Sources/widgets/spinnerwidget.cpp \
    Sources/widgets/mainwindow.cpp \
    Sources/widgets/basepage.cpp \
    Sources/widgets/pages/loadingpage.cpp \
    Sources/struct/gamemanifest.cpp \
    Sources/struct/manifestfile.cpp \
    Sources/widgets/pages/playpage.cpp \
    Sources/pathhelper.cpp \
    Sources/struct/appstate.cpp \
    Sources/states/recoverableerror.cpp \
    Sources/states/fatalerror.cpp \
    Sources/app.cpp \
    Sources/states/serverchooser.cpp \
    Sources/states/filechecker.cpp \
    Sources/states/fileretriever.cpp \
    Sources/states/settingseditor.cpp \
    Sources/states/gamerunner.cpp \
    Sources/workers/downloadworker.cpp \
    Sources/workers/filecheckworker.cpp \
    Sources/workers/rungameworker.cpp \
    Sources/widgets/pages/runningpage.cpp \
    Sources/widgets/pages/loginpage.cpp \
    Sources/widgets/pages/errorpage.cpp \
    Sources/states/loadmanifest.cpp \
    Sources/states/login.cpp \
    Sources/struct/config.cpp \
    Sources/struct/style.cpp \
    Sources/widgets/pages/settingspage.cpp

HEADERS  += \
    Sources/widgets/slidewidget.h \
    Sources/widgets/spinnerwidget.h \
    Sources/widgets/mainwindow.h \
    Sources/widgets/basepage.h \
    Sources/widgets/pages/loadingpage.h \
    Sources/struct/gamemanifest.h \
    Sources/struct/manifestfile.h \
    Sources/widgets/pages/playpage.h \
    Sources/pathhelper.h \
    Sources/struct/appstate.h \
    Sources/states/recoverableerror.h \
    Sources/states/fatalerror.h \
    Sources/app.h \
    Sources/states/serverchooser.h \
    Sources/states/filechecker.h \
    Sources/states/gamerunner.h \
    Sources/states/fileretriever.h \
    Sources/states/settingseditor.h \
    Sources/workers/downloadworker.h \
    Sources/workers/filecheckworker.h \
    Sources/workers/rungameworker.h \
    Sources/widgets/pages/runningpage.h \
    Sources/widgets/pages/loginpage.h \
    Sources/widgets/pages/errorpage.h \
    Sources/states/loadmanifest.h \
    Sources/states/login.h \
    Sources/struct/config.h \
    Sources/struct/style.h \
    Sources/widgets/pages/settingspage.h

FORMS    += \
    Sources/widgets/mainwindow.ui \
    Sources/widgets/pages/loadingpage.ui \
    Sources/widgets/pages/playpage.ui \
    Sources/widgets/pages/runningpage.ui \
    Sources/widgets/pages/loginpage.ui \
    Sources/widgets/pages/errorpage.ui \
    Sources/widgets/pages/settingspage.ui

RESOURCES += \
    Resources/resources.qrc

DISTFILES +=
