#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QString>
#include <QSettings>

class Config : public QObject {
Q_OBJECT
public:
    explicit Config(QObject *parent = 0);
    ~Config();

    QString getUsername();
    QString getUserId();
    QString getAccessToken();
    QString getXmx();
    QString getLastServer();
    void setUsername(QString value);
    void setUserId(QString value);
    void setAccessToken(QString value);
    void setXmx(QString value);
    void setLastServer(QString value);
    void setRemember(bool value);

protected:
    void loadConfig();
    void saveConfig();

    QString m_username;
    QString m_userId;
    QString m_accessToken;
    QString m_xmx;
    QString m_lastServer;
    bool m_remember;

    QSettings *m_settings;
};

#endif // CONFIG_H
