#include <QJsonArray>
#include "struct/manifestfile.h"
#include "pathhelper.h"

ManifestFile::ManifestFile(QJsonObject object, GameManifest *manifest, FileType type) {
    if (object["url"] != QJsonValue::Undefined)
        this->url = object["url"].toString();
    if (object["sha1"] != QJsonValue::Undefined)
        this->sha1 = object["sha1"].toString();
    if (object["name"] != QJsonValue::Undefined)
        this->name = object["name"].toString();
    if (object["extract"] != QJsonValue::Undefined)
        this->extract = object["extract"].toBool();
    if (object["files"] != QJsonValue::Undefined) {
        QJsonArray files = object["files"].toArray();
        for (int i = 0; i < files.count(); i++) {
            this->files.push_back(ManifestFile(files[i].toObject(), manifest, NativeLibrary));
        }
    }

    switch (type) {
    case ClientJar:
        this->localPath = PathHelper::versionPath(manifest->id);
        break;
    case LibraryJar:
        this->localPath = PathHelper::libraryPath(this->name);
        break;
    case NativeLibrary:
        this->localPath = PathHelper::nativePath(manifest->id, this->name);
        break;
    case AssetsJar:
        this->localPath = PathHelper::assetsPath(manifest->id);
        break;
    }
}

