#ifndef APPSTATE_H
#define APPSTATE_H

#include <QObject>
#include <QString>
#include <QList>
#include "struct/config.h"
#include "struct/gamemanifest.h"
#include "struct/manifestfile.h"
#include "widgets/mainwindow.h"

class AppState : public QObject {
Q_OBJECT
public:
    explicit AppState(QObject *parent = 0);
    ~AppState();

    static AppState *getInstance();

    // manifests
    QList<GameManifest *> *getManifests();
    GameManifest *getManifest(int index);
    GameManifest *getCurrentManifest();
    int getManifestsCount();
    void addManifest(GameManifest *manifest);

    // error
    QString getLastError();
    void setError(QString message);

    // log
    QString getLog();
    void appendLog(QString data);
    void clearLog();

    // missing files
    QList<ManifestFile *> *getMissingFiles();
    ManifestFile *getMissingFile(int index);
    int getMissingFilesCount();
    void addMissingFile(ManifestFile *manifestFile);
    void clearMissingFiles();

    // current server
    int getCurrentServer();
    void setCurrentServer(int index);

    Config *getConfig();
    enum ReturnState { Login, ServerChooser };

    AppState::ReturnState getReturnState() const;
    void setReturnState(const ReturnState &returnState);

signals:
    void logUpdated();

protected:
    QList<GameManifest *> m_manifests;
    QList<ManifestFile *> m_missingFiles;
    QString m_error;
    QString m_log;
    int m_currentServer;
    Config *m_config;
    ReturnState m_returnState = Login;

    static AppState *m_objectPointer;
};

#endif // APPSTATE_H
