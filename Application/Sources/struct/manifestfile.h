#ifndef MANIFESTFILE_H
#define MANIFESTFILE_H

#include <QJsonObject>
#include <QList>

class GameManifest;

class ManifestFile {
public:
    enum FileType { ClientJar, LibraryJar, NativeLibrary, AssetsJar };

    explicit ManifestFile() {}
    ManifestFile(QJsonObject object, GameManifest *manifest, FileType type);

    QString url = "", sha1 = "", name = "", localPath = "";
    QList<ManifestFile> files;
    bool extract = false;
};

#endif // MANIFESTFILE_H
