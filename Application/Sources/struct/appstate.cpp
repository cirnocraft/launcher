#include "appstate.h"

AppState *AppState::m_objectPointer;

AppState::AppState(QObject *parent) : QObject(parent) {
    m_objectPointer = this;
    m_config = new Config();
}

AppState::~AppState() {
    delete m_config;
}

AppState *AppState::getInstance() {
    return m_objectPointer;
}

QList<GameManifest *> *AppState::getManifests() {
    return &m_manifests;
}

GameManifest *AppState::getManifest(int index) {
    return m_manifests[index];
}

GameManifest *AppState::getCurrentManifest() {
    return m_manifests[getCurrentServer()];
}

int AppState::getManifestsCount() {
    return m_manifests.count();
}

void AppState::addManifest(GameManifest *manifest) {
    m_manifests.push_back(manifest);
}

QString AppState::getLastError() {
    return m_error;
}

void AppState::setError(QString message) {
    m_error = message;
}

QString AppState::getLog() {
    return m_log;
}

void AppState::appendLog(QString data) {
    m_log += data;
    emit logUpdated();
}

void AppState::clearLog() {
    m_log = "";
    emit logUpdated();
}

QList<ManifestFile *> *AppState::getMissingFiles() {
    return &m_missingFiles;
}

ManifestFile *AppState::getMissingFile(int index) {
    return m_missingFiles[index];
}

int AppState::getMissingFilesCount() {
    return m_missingFiles.count();
}

void AppState::addMissingFile(ManifestFile *missingFile) {
    m_missingFiles.push_back(missingFile);
}

void AppState::clearMissingFiles() {
    m_missingFiles.clear();
}

int AppState::getCurrentServer() {
    return m_currentServer;
}

void AppState::setCurrentServer(int index) {
    m_currentServer = index;
}

Config *AppState::getConfig() {
    return m_config;
}

AppState::ReturnState AppState::getReturnState() const
{
    return m_returnState;
}

void AppState::setReturnState(const ReturnState &returnState)
{
    m_returnState = returnState;
}
