#include <QDir>
#include "config.h"
#include "pathhelper.h"

Config::Config(QObject *parent) : QObject(parent) {
    m_settings = new QSettings(QDir(PathHelper::baseDir()).filePath("settings.ini"), QSettings::IniFormat);
    loadConfig();
}

Config::~Config() {
    delete m_settings;
}

QString Config::getUsername() {
    return m_username;
}

QString Config::getUserId() {
    return m_userId;
}

QString Config::getAccessToken() {
    return m_accessToken;
}

QString Config::getXmx() {
    return m_xmx;
}

QString Config::getLastServer() {
    return m_lastServer;
}

void Config::setUsername(QString value) {
    m_username = value;
    saveConfig();
}

void Config::setUserId(QString value) {
    m_userId = value;
    saveConfig();
}

void Config::setAccessToken(QString value) {
    m_accessToken = value;
    saveConfig();
}

void Config::setXmx(QString value) {
    m_xmx = value;
    saveConfig();
}

void Config::setLastServer(QString value) {
    m_lastServer = value;
    saveConfig();
}

void Config::setRemember(bool value) {
    m_remember = value;
}

void Config::loadConfig() {
    m_username = m_settings->value("username", "").toString();
    m_userId = m_settings->value("userId", "").toString();
    m_xmx = m_settings->value("xmx", "1024").toString();
    // m_lastServer = m_settings->value("lastServer", "").toString();
    m_accessToken = m_settings->value("accessToken", "").toString();
    m_remember = !m_accessToken.isEmpty();
}

void Config::saveConfig() {
    m_settings->setValue("username", m_username);
    m_settings->setValue("userId", m_userId);
    m_settings->setValue("xmx", m_xmx);
    // m_settings->setValue("lastServer", m_lastServer);
    if (m_remember)
        m_settings->setValue("accessToken", m_accessToken);
    else
        m_settings->setValue("accessToken", "");
}
