#ifndef GAMEMANIFEST_H
#define GAMEMANIFEST_H

#include <QList>
#include "struct/manifestfile.h"

class GameManifest {
public:
    explicit GameManifest() {}

    QString id, name, baseVersion, assetsVersion, mainClass, args, server, port;
    ManifestFile assets, client;
    QList<ManifestFile> libraries, natives;
};

#endif // GAMEMANIFEST_H
