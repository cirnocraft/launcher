#include <QSignalTransition>
#include <QList>

#include "struct/manifestfile.h"
#include "app.h"

App::App(QObject *parent) : QObject(parent) {
    m_stateMachine = new QStateMachine();

    // create states
    m_loginState = new QState();
    m_loadManifestState = new QState();
    m_serverChooserState = new QState();
    m_settingsEditorState = new QState();
    m_fileCheckerState = new QState();
    m_fileRetrieverState = new QState();
    m_gameRunnerState = new QState();
    m_recoverableErrorState = new QState();

    // create workers
    m_login = new Login();
    m_loadManifest = new LoadManifest();
    m_serverChooser = new ServerChooser();
    m_settingsEditor = new SettingsEditor();
    m_fileChecker = new FileChecker();
    m_fileRetriever = new FileRetriever();
    m_gameRunner = new GameRunner();
    m_recoverableError = new RecoverableError(m_loginState, m_serverChooserState);

    // connect entered
    QObject::connect(m_loginState, SIGNAL(entered()), m_login, SLOT(entered()));
    QObject::connect(m_loadManifestState, SIGNAL(entered()), m_loadManifest, SLOT(entered()));
    QObject::connect(m_serverChooserState, SIGNAL(entered()), m_serverChooser, SLOT(entered()));
    QObject::connect(m_settingsEditorState, SIGNAL(entered()), m_settingsEditor, SLOT(entered()));
    QObject::connect(m_fileCheckerState, SIGNAL(entered()), m_fileChecker, SLOT(entered()));
    QObject::connect(m_fileRetrieverState, SIGNAL(entered()), m_fileRetriever, SLOT(entered()));
    QObject::connect(m_gameRunnerState, SIGNAL(entered()), m_gameRunner, SLOT(entered()));
    QObject::connect(m_recoverableErrorState, SIGNAL(entered()), m_recoverableError, SLOT(entered()));

    // connect exited
    QObject::connect(m_loginState, SIGNAL(exited()), m_login, SLOT(exited()));
    QObject::connect(m_loadManifestState, SIGNAL(exited()), m_loadManifest, SLOT(exited()));
    QObject::connect(m_serverChooserState, SIGNAL(exited()), m_serverChooser, SLOT(exited()));
    QObject::connect(m_settingsEditorState, SIGNAL(exited()), m_settingsEditor, SLOT(exited()));
    QObject::connect(m_fileCheckerState, SIGNAL(exited()), m_fileChecker, SLOT(exited()));
    QObject::connect(m_fileRetrieverState, SIGNAL(exited()), m_fileRetriever, SLOT(exited()));
    QObject::connect(m_gameRunnerState, SIGNAL(exited()), m_gameRunner, SLOT(exited()));
    QObject::connect(m_recoverableErrorState, SIGNAL(exited()), m_recoverableError, SLOT(exited()));

    // set transitions
    // from Login
    m_loginState->addTransition(m_login, SIGNAL(finished()), m_loadManifestState);
    m_loginState->addTransition(m_login, SIGNAL(error()), m_recoverableErrorState);
    // from LoadManifest
    m_loadManifestState->addTransition(m_loadManifest, SIGNAL(finished()), m_serverChooserState);
    m_loadManifestState->addTransition(m_loadManifest, SIGNAL(error()), m_recoverableErrorState);
    // from ServerChooser
    m_serverChooserState->addTransition(m_serverChooser, SIGNAL(runGame()), m_fileCheckerState);
    m_serverChooserState->addTransition(m_serverChooser, SIGNAL(openSettings()), m_settingsEditorState);
    // from SettingsEditor
    m_settingsEditorState->addTransition(m_settingsEditor, SIGNAL(finished()), m_serverChooserState);
    m_settingsEditorState->addTransition(m_settingsEditor, SIGNAL(logout()), m_loginState);
    // from FileChecker
    m_fileCheckerState->addTransition(m_fileChecker, SIGNAL(downloadNeeded()), m_fileRetrieverState);
    m_fileCheckerState->addTransition(m_fileChecker, SIGNAL(finished()), m_gameRunnerState);
    // from FileRetriever
    m_fileRetrieverState->addTransition(m_fileRetriever, SIGNAL(finished()), m_gameRunnerState);
    m_fileRetrieverState->addTransition(m_fileRetriever, SIGNAL(error()), m_recoverableErrorState);
    // from GameRunner
    m_gameRunnerState->addTransition(m_gameRunner, SIGNAL(finished()), m_serverChooserState);
    m_gameRunnerState->addTransition(m_gameRunner, SIGNAL(error()), m_recoverableErrorState);
    // from RecoverableError
    m_recoverableErrorState->addTransition(m_recoverableError, SIGNAL(toServerChooser()), m_serverChooserState);
    m_recoverableErrorState->addTransition(m_recoverableError, SIGNAL(toLogin()), m_loginState);

    // add states to state machine
    m_stateMachine->addState(m_loginState);
    m_stateMachine->addState(m_loadManifestState);
    m_stateMachine->addState(m_serverChooserState);
    m_stateMachine->addState(m_settingsEditorState);
    m_stateMachine->addState(m_fileCheckerState);
    m_stateMachine->addState(m_fileRetrieverState);
    m_stateMachine->addState(m_gameRunnerState);
    m_stateMachine->addState(m_recoverableErrorState);

    // set initial state
    m_stateMachine->setInitialState(m_loginState);
}

App::~App() {
    delete m_stateMachine;

    delete m_loadManifest;
    delete m_serverChooser;
    delete m_settingsEditor;
    delete m_fileChecker;
    delete m_fileRetriever;
    delete m_gameRunner;
    delete m_recoverableError;
}

void App::start() {
    m_stateMachine->start();
}
