#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMainWindow>
#include <QString>
#include "widgets/basepage.h"
#include "widgets/slidewidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
Q_OBJECT
public:
    enum Pages : int { Loading, Login, ServerChooser, Settings, GameLog, Error };
    Q_ENUMS(Pages)

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static MainWindow *getInstance();
    BasePage *getPage(Pages index);

    void slideToPage(Pages page, SlideWidget::Direction direction = SlideWidget::Automatic);
    void goToPage(Pages page);
    void showLoading(QString line1, QString line2 = "", QString line3 = "");

protected:
    static MainWindow *objectPointer;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
