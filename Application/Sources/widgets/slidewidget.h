#ifndef SLIDEWIDGET_H
#define SLIDEWIDGET_H

#include <QWidget>
#include <QStackedWidget>
#include <QEasingCurve>
#include <QList>
#include <QPair>

class SlideWidget : public QStackedWidget {
Q_OBJECT
Q_PROPERTY(int speed READ speed WRITE setSpeed)
Q_PROPERTY(QEasingCurve::Type animationType READ animationType WRITE setAnimationType)
public:
    explicit SlideWidget(QWidget *parent = 0);
    ~SlideWidget();

    enum Direction { LeftToRight, RightToLeft, Automatic };
    Q_ENUMS(Direction)

    int speed();
    void setSpeed(int speed);
    QEasingCurve::Type animationType();
    void setAnimationType(enum QEasingCurve::Type animationType);

    void slideInPage(int page, enum Direction direction = Automatic);

signals:
    void animationFinished(void);

protected slots:
    void animationDoneSlot(void);

protected:
    int m_speed, m_now, m_next;
    bool m_active = false;
    QEasingCurve::Type m_animationType;
    QWidget *m_mainWindow;
    QList<QPair<QWidget*, Direction>> m_queue;

    void slideInWidget(QWidget *widget, Direction direction);
};

#endif // SLIDEWIDGET_H
