#ifndef PLAYPAGE_H
#define PLAYPAGE_H

#include <QWidget>
#include <QStringList>
#include "widgets/basepage.h"


namespace Ui {
    class PlayPage;
}

class ServerChooserPage : public BasePage {
Q_OBJECT
public:
    explicit ServerChooserPage(QWidget *parent = 0);
    ~ServerChooserPage();

    void initServersList(QStringList list);

signals:
    void playClicked(void);
    void settingsClicked(void);

protected slots:
    void serverChanged(int);

private:
    Ui::PlayPage *ui;
};

#endif // PLAYPAGE_H
