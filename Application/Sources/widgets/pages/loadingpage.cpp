#include <QLabel>
#include "loadingpage.h"
#include "ui_loadingpage.h"

LoadingPage::LoadingPage(QWidget *parent) :
             BasePage(parent),
             ui(new Ui::LoadingPage) {
    ui->setupUi(this);
}

LoadingPage::~LoadingPage() {
    delete ui;
}

void LoadingPage::setMessage(QString line1, QString line2, QString line3) {
    ui->label->setText(line1);
    if (!line2.isEmpty())
        ui->label2->setText(line2);
    if (!line3.isEmpty())
        ui->label3->setText(line3);
}
