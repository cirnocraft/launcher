#ifndef LOADINGPAGE_H
#define LOADINGPAGE_H

#include <QWidget>
#include <QString>
#include "widgets/basepage.h"


namespace Ui {
    class LoadingPage;
}

class LoadingPage : public BasePage {
Q_OBJECT
public:
    explicit LoadingPage(QWidget *parent = 0);
    ~LoadingPage();

    void setMessage(QString line1, QString line2, QString line3);

private:
    Ui::LoadingPage *ui;
};

#endif // LOADINGPAGE_H
