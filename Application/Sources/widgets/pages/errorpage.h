#ifndef ERRORPAGE_H
#define ERRORPAGE_H

#include <QWidget>
#include <QShowEvent>
#include "widgets/basepage.h"

namespace Ui {
class ErrorPage;
}

class ErrorPage : public BasePage {
Q_OBJECT

public:
    explicit ErrorPage(QWidget *parent = 0);
    ~ErrorPage();

signals:
    void returnBack();

protected:
    void showEvent(QShowEvent *event);

private:
    Ui::ErrorPage *ui;
};

#endif // ERRORPAGE_H
