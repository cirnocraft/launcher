#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include "loginpage.h"
#include "ui_loginpage.h"

LoginPage::LoginPage(QWidget *parent) :
    BasePage(parent),
    ui(new Ui::LoginPage) {
    ui->setupUi(this);
    QObject::connect(ui->btnSignIn, SIGNAL(clicked()), this, SIGNAL(signIn()));
}

LoginPage::~LoginPage() {
    delete ui;
}

QJsonObject LoginPage::getData() {
    QJsonObject data;
    data["username"] = ui->edtUsername->text();
    data["password"] = ui->edtPassword->text();
    return data;
}

bool LoginPage::getRemember() {
    return ui->chkRemember->isChecked();
}

void LoginPage::setUsername(QString value) {
    ui->edtUsername->setText(value);
}

void LoginPage::clearPassword() {
    ui->edtPassword->setText("");
    ui->chkRemember->setChecked(false);
}
