#include <QLabel>
#include <QPushButton>
#include "widgets/pages/errorpage.h"
#include "struct/appstate.h"
#include "ui_errorpage.h"

ErrorPage::ErrorPage(QWidget *parent) :
    BasePage(parent),
    ui(new Ui::ErrorPage) {
    ui->setupUi(this);
    QObject::connect(ui->btnBack, SIGNAL(clicked()), this, SIGNAL(returnBack()));
}

ErrorPage::~ErrorPage() {
    delete ui;
}

void ErrorPage::showEvent(QShowEvent *) {
    ui->lblMessage->setText(AppState::getInstance()->getLastError());
}
