#include <QPlainTextEdit>
#include <QScrollBar>
#include <QPushButton>
#include "widgets/pages/runningpage.h"
#include "ui_runningpage.h"
#include "struct/appstate.h"

GameLogPage::GameLogPage(QWidget *parent) :
    BasePage(parent),
    ui(new Ui::RunningPage) {
    ui->setupUi(this);
    QObject::connect(ui->btnBack, SIGNAL(clicked()), this, SIGNAL(goBack()));
}

GameLogPage::~GameLogPage() {
    delete ui;
}

void GameLogPage::setBackBtnVisible(bool val) {
    ui->btnBack->setVisible(val);
}

void GameLogPage::logUpdated() {
    int pos = ui->txtLog->verticalScrollBar()->sliderPosition();
    bool max = pos == ui->txtLog->verticalScrollBar()->maximum();
    ui->txtLog->clear();
    ui->txtLog->appendPlainText(AppState::getInstance()->getLog());
    if (max) {
        ui->txtLog->verticalScrollBar()->
                setSliderPosition(ui->txtLog->verticalScrollBar()->maximum());
    } else {
        ui->txtLog->verticalScrollBar()->setSliderPosition(pos);
    }
}

void GameLogPage::showEvent(QShowEvent*) {
    QObject::connect(AppState::getInstance(), SIGNAL(logUpdated()),
                     this, SLOT(logUpdated()));
    logUpdated();
}

void GameLogPage::hideEvent(QHideEvent*) {
    QObject::disconnect(AppState::getInstance(), SIGNAL(logUpdated()),
                        this, SLOT(logUpdated()));
}
