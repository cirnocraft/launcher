#ifndef RUNNINGPAGE_H
#define RUNNINGPAGE_H

#include <QWidget>
#include <QShowEvent>
#include <QHideEvent>
#include "widgets/basepage.h"

namespace Ui {
class RunningPage;
}

class GameLogPage : public BasePage {
Q_OBJECT

public:
    explicit GameLogPage(QWidget *parent = 0);
    ~GameLogPage();
    void setBackBtnVisible(bool val);

signals:
    void goBack();

protected slots:
    void logUpdated();

protected:
    void showEvent(QShowEvent*);
    void hideEvent(QHideEvent*);

private:
    Ui::RunningPage *ui;
};

#endif // RUNNINGPAGE_H
