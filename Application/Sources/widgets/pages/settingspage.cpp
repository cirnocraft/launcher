#include <QPushButton>
#include <QComboBox>
#include <QStyledItemDelegate>
#include "settingspage.h"
#include "ui_settingspage.h"

SettingsPage::SettingsPage(QWidget *parent) :
    BasePage(parent),
    ui(new Ui::SettingsPage) {
    ui->setupUi(this);
    connect(ui->btnBack, SIGNAL(clicked()), this, SIGNAL(finished()));
    connect(ui->btnLogout, SIGNAL(clicked()), this, SIGNAL(logout()));
    connect(ui->cmbXmx, SIGNAL(currentIndexChanged(int)), this, SIGNAL(xmxChanged(int)));

    QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
    ui->cmbXmx->setItemDelegate(itemDelegate);
}

SettingsPage::~SettingsPage() {
    delete ui;
}

void SettingsPage::setXmxList(QStringList list) {
    ui->cmbXmx->addItems(list);
}

void SettingsPage::setCurrentXmx(int index) {
    ui->cmbXmx->setCurrentIndex(index);
}
