#ifndef SETTINGSPAGE_H
#define SETTINGSPAGE_H

#include <QWidget>
#include <QStringList>

#include "widgets/basepage.h"

namespace Ui {
class SettingsPage;
}

class SettingsPage : public BasePage {
Q_OBJECT

public:
    explicit SettingsPage(QWidget *parent = 0);
    ~SettingsPage();

    void setXmxList(QStringList list);
    void setCurrentXmx(int index);

signals:
    void finished();
    void logout();
    void xmxChanged(int);

private:
    Ui::SettingsPage *ui;
};

#endif // SETTINGSPAGE_H
