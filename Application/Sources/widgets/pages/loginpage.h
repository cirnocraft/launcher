#ifndef LOGINPAGE_H
#define LOGINPAGE_H

#include <QJsonObject>
#include <QWidget>

#include "widgets/basepage.h"

namespace Ui {
class LoginPage;
}

class LoginPage : public BasePage {
Q_OBJECT

public:
    explicit LoginPage(QWidget *parent = 0);
    ~LoginPage();

    QJsonObject getData();
    bool getRemember();
    QString getUsername();
    void setUsername(QString value);
    QString getPassword();
    void clearPassword();

signals:
    void signIn();

private:
    Ui::LoginPage *ui;
};

#endif // LOGINPAGE_H
