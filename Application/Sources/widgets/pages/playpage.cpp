#include <QComboBox>
#include <QPushButton>
#include <QStyledItemDelegate>
#include "widgets/pages/playpage.h"
#include "ui_playpage.h"
#include "struct/appstate.h"

ServerChooserPage::ServerChooserPage(QWidget *parent) :
          BasePage(parent),
          ui(new Ui::PlayPage) {
    ui->setupUi(this);

    QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
    ui->cmbServer->setItemDelegate(itemDelegate);

    QObject::connect(ui->btnPlay, SIGNAL(clicked()),
                     this, SIGNAL(playClicked()));
    QObject::connect(ui->btnSettings, SIGNAL(clicked()),
                     this, SIGNAL(settingsClicked()));
    QObject::connect(ui->cmbServer, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(serverChanged(int)));
}

ServerChooserPage::~ServerChooserPage() {
    delete ui;
}

void ServerChooserPage::initServersList(QStringList list) {
    ui->cmbServer->clear();
    for (int i = 0; i < list.size(); i++) {
        ui->cmbServer->addItem(list[i]);
    }
}

void ServerChooserPage::serverChanged(int index) {
    AppState::getInstance()->setCurrentServer(index);
}
