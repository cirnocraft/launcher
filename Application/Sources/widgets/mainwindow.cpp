#include <QUrl>
#include <QFile>
#include <QTextStream>
#include <QFontDatabase>
#include <QDebug>
#include "widgets/pages/settingspage.h"
#include "widgets/mainwindow.h"
#include "ui_mainwindow.h"
#include "struct/appstate.h"
#include "widgets/pages/loadingpage.h"
#include "widgets/pages/loginpage.h"
#include "widgets/pages/playpage.h"
#include "widgets/pages/runningpage.h"
#include "widgets/pages/errorpage.h"

MainWindow *MainWindow::objectPointer;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    QFile *css = new QFile(":/style.css");
    if (css->exists()) {
        css->open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(css);
        this->setStyleSheet(ts.readAll());
    }
    int id = QFontDatabase::addApplicationFont(":/Minecraft.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QApplication::setFont(family);

    ui->slideWidget->addWidget(new LoadingPage(this));
    ui->slideWidget->addWidget(new LoginPage(this));
    ui->slideWidget->addWidget(new ServerChooserPage(this));
    ui->slideWidget->addWidget(new SettingsPage(this));
    ui->slideWidget->addWidget(new GameLogPage(this));
    ui->slideWidget->addWidget(new ErrorPage(this));
    ui->slideWidget->setCurrentIndex(Pages::Login);

    objectPointer = this;
}

MainWindow::~MainWindow() {
    delete ui;
}

MainWindow *MainWindow::getInstance() {
    return objectPointer;
}

BasePage *MainWindow::getPage(MainWindow::Pages index) {
    return static_cast<BasePage *>(ui->slideWidget->widget(index));
}

void MainWindow::slideToPage(Pages page, SlideWidget::Direction direction) {
    ui->slideWidget->slideInPage(page, direction);
}

void MainWindow::goToPage(MainWindow::Pages page) {
    ui->slideWidget->setCurrentIndex(page);
}

void MainWindow::showLoading(QString line1, QString line2, QString line3) {
    static_cast<LoadingPage *>(getPage(Loading))->setMessage(line1, line2, line3);
    ui->slideWidget->slideInPage(Loading, SlideWidget::RightToLeft);
}
