#ifndef BASEPAGE_H
#define BASEPAGE_H

#include <QObject>
#include <QWidget>
#include <QPaintEvent>

class BasePage : public QWidget
{
Q_OBJECT
Q_PROPERTY(int x READ x WRITE setX)
public:
    explicit BasePage(QWidget *parent = 0);
    ~BasePage();

    void setX(int value);
protected:
    void paintEvent(QPaintEvent *evt);
};

#endif // BASEPAGE_H
