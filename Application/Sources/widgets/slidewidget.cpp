#include <QDebug>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include "slidewidget.h"

SlideWidget::SlideWidget(QWidget *parent) : QStackedWidget(parent) {
    if (parent != 0) {
        m_mainWindow = parent;
    } else {
        m_mainWindow = this;
        qWarning() << "SlideWidget: No parent";
    }

    m_speed = 500;
    m_animationType = QEasingCurve::OutQuad;
    m_now = 0;
}

SlideWidget::~SlideWidget() {}

int SlideWidget::speed() {
    return m_speed;
}

void SlideWidget::setSpeed(int speed) {
    m_speed = speed;
}

QEasingCurve::Type SlideWidget::animationType() {
    return m_animationType;
}

void SlideWidget::setAnimationType(QEasingCurve::Type animationType) {
    m_animationType = animationType;
}

void SlideWidget::slideInPage(int page, Direction direction) {
    if (page < count()) {
        this->slideInWidget(widget(page), direction);
    }
}

void SlideWidget::slideInWidget(QWidget *nextWidget, Direction direction) {
    if (m_active) {
        m_queue.push_back(QPair<QWidget*, Direction>(nextWidget, direction));
        return;
    }

    m_active = true;
    enum Direction directionHint;
    int now = currentIndex();
    int next = indexOf(nextWidget);
    if (now == next) {
        m_active = false;
        return;
    }
    if (direction == Automatic) {
        directionHint = now < next ? RightToLeft : LeftToRight;
    } else {
        directionHint = direction;
    }

    int offset = this->frameRect().width();

    if (directionHint == LeftToRight) {
        offset *= -1;
    }
    widget(next)->resize(this->size());
    widget(next)->move(offset, 0);
    widget(next)->show();
    widget(next)->raise();

    QPropertyAnimation *animNow = new QPropertyAnimation(widget(now), "x");
    animNow->setDuration(m_speed);
    animNow->setEasingCurve(m_animationType);
    animNow->setStartValue(0);
    animNow->setEndValue(-offset / 3);
    QPropertyAnimation *animNext = new QPropertyAnimation(widget(next), "x");
    animNext->setDuration(m_speed);
    animNext->setEasingCurve(m_animationType);
    animNext->setStartValue(offset);
    animNext->setEndValue(0);

    QParallelAnimationGroup *animGroup = new QParallelAnimationGroup;
    animGroup->addAnimation(animNow);
    animGroup->addAnimation(animNext);
    QObject::connect(animGroup, SIGNAL(finished()), this, SLOT(animationDoneSlot()));
    m_next = next;
    m_now = now;
    animGroup->start();
}

void SlideWidget::animationDoneSlot(void) {
    setCurrentIndex(m_next);
    widget(m_now)->hide();
    widget(m_now)->move(0, 0);
    m_active = false;
    m_now = m_next;
    if (m_queue.count() > 0) {
        QPair<QWidget*, Direction> args = m_queue.front();
        m_queue.pop_front();
        slideInWidget(args.first, args.second);
    }
}
