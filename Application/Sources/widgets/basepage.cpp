#include <QDebug>
#include <QPainter>
#include <QStyle>
#include <QStyleOption>
#include "basepage.h"

BasePage::BasePage(QWidget *parent) : QWidget(parent) {}

BasePage::~BasePage() {
}

void BasePage::setX(int value) {
    move(QPoint(value, y()));
}

void BasePage::paintEvent(QPaintEvent *) {
    QStyleOption *op = new QStyleOption();
    op->initFrom(this);

    QPainter painter(this);
    this->style()->drawPrimitive(QStyle::PE_Widget, op, &painter, this);
}
