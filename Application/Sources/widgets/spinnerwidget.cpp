#include <QDebug>
#include <QtMath>
#include <QPainter>
#include <QBrush>
#include <QConicalGradient>
#include <QColor>
#include "spinnerwidget.h"

SpinnerWidget::SpinnerWidget(QWidget *parent) : QWidget(parent) {
    m_animation = new QVariantAnimation();
    m_animation->setDuration(1000);
    m_animation->setStartValue(360);
    m_animation->setEndValue(0);
    m_animation->setLoopCount(-1);
    QObject::connect(m_animation, SIGNAL(valueChanged(const QVariant&)), this, SLOT(angleUpdated()));
}

SpinnerWidget::~SpinnerWidget() {
    delete m_animation;
}

void SpinnerWidget::angleUpdated(void) {
    repaint();
}

QPen SpinnerWidget::getPen(QRect rect, int angle) {
    angle = (angle - 20 + 360) % 360;
    QConicalGradient gradient(rect.center(), angle + SPAN);
    gradient.setColorAt(0, QColor(255, 255, 255, 255));
    gradient.setColorAt(qFabs(SPAN) / 360, QColor(255, 255, 255, 0));

    QPen pen(QBrush(gradient), LINE_WIDTH);
    pen.setCapStyle(Qt::RoundCap);
    return pen;
}

void SpinnerWidget::paintEvent(QPaintEvent *evt) {
    int angle = m_animation->currentValue().toInt();
    QRect rect(
        (evt->rect().width() - SIZE + LINE_WIDTH) / 2,
        LINE_WIDTH / 2,
        SIZE - LINE_WIDTH,
        SIZE - LINE_WIDTH
    );
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(getPen(rect, angle));
    painter.drawArc(rect, angle*16, SPAN*16);
    painter.end();
}

QSize SpinnerWidget::sizeHint() {
    return QSize(SIZE, SIZE);
}

void SpinnerWidget::showEvent(QShowEvent*) {
    if (m_animation != nullptr && m_animation->state() == QVariantAnimation::Stopped) {
        m_animation->start();
    }
}

void SpinnerWidget::hideEvent(QHideEvent*) {
    if (m_animation != nullptr && m_animation->state() == QVariantAnimation::Running) {
        m_animation->stop();
    }
}
