#ifndef SPINNERWIDGET_H
#define SPINNERWIDGET_H

#include <QWidget>
#include <QVariantAnimation>
#include <QPen>
#include <QRect>
#include <QSize>
#include <QPaintEvent>
#include <QHideEvent>
#include <QShowEvent>

class SpinnerWidget : public QWidget {
Q_OBJECT
public:
    explicit SpinnerWidget(QWidget *parent = 0);
    ~SpinnerWidget();

protected slots:
    void angleUpdated(void);

protected:
    const int SIZE = 50,
              SPAN = -180,
              LINE_WIDTH = 10;
    QVariantAnimation *m_animation;

    QPen getPen(QRect rect, int angle);
    void paintEvent(QPaintEvent *evt);
    QSize sizeHint();
    void showEvent(QShowEvent*);
    void hideEvent(QHideEvent*);
};

#endif // SPINNERWIDGET_H
