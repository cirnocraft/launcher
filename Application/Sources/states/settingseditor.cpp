#include "settingseditor.h"
#include "widgets/mainwindow.h"
#include "widgets/pages/settingspage.h"
#include "struct/appstate.h"
#include "struct/config.h"

SettingsEditor::SettingsEditor(QObject *parent) : QObject(parent) {
    SettingsPage *settingsPage = static_cast<SettingsPage*>(MainWindow::getInstance()->getPage(MainWindow::Settings));
    settingsPage->setXmxList(m_xmxLabels);
    connect(settingsPage, SIGNAL(logout()), this, SLOT(doLogout()));
    connect(settingsPage, SIGNAL(finished()), this, SIGNAL(finished()));
    connect(settingsPage, SIGNAL(xmxChanged(int)), this, SLOT(xmxChanged(int)));
}

SettingsEditor::~SettingsEditor() {

}

void SettingsEditor::entered() {
    SettingsPage *settingsPage = static_cast<SettingsPage*>(MainWindow::getInstance()->getPage(MainWindow::Settings));
    int xmxIndex = m_xmxValues.indexOf(AppState::getInstance()->getConfig()->getXmx());
    settingsPage->setCurrentXmx(xmxIndex >= 0 ? xmxIndex : 0);
    MainWindow::getInstance()->slideToPage(MainWindow::Settings);
}

void SettingsEditor::exited() {

}

void SettingsEditor::doLogout() {
    AppState::getInstance()->getConfig()->setAccessToken("");
    emit logout();
}

void SettingsEditor::xmxChanged(int index) {
    AppState::getInstance()->getConfig()->setXmx(m_xmxValues[index]);
}
