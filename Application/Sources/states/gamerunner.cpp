#include "states/gamerunner.h"
#include "widgets/mainwindow.h"
#include "widgets/pages/runningpage.h"
#include "struct/appstate.h"

GameRunner::GameRunner(QObject *parent) : QObject(parent) {
    m_runGameWorker = new RunGameWorker();
    QObject::connect(m_runGameWorker, SIGNAL(errorOccurred(QProcess::ProcessError)),
                     this, SLOT(errorOccurred(QProcess::ProcessError)));
    QObject::connect(m_runGameWorker, SIGNAL(finished(int)),
                     this, SLOT(processClosed(int)));
    GameLogPage *runningPage = static_cast<GameLogPage*>(MainWindow::getInstance()->getPage(MainWindow::GameLog));
    QObject::connect(runningPage, SIGNAL(goBack()), this, SIGNAL(finished()));
}

GameRunner::~GameRunner() {
    delete m_runGameWorker;
}

void GameRunner::entered() {
    AppState::getInstance()->setReturnState(AppState::ServerChooser);
    MainWindow::getInstance()->showLoading(tr("Running game..."));
#if defined(DEBUG_VERSION)
    MainWindow::getInstance()->slideToPage(MainWindow::GameLog);
    GameLogPage *runningPage = static_cast<GameLogPage*>(MainWindow::getInstance()->getPage(MainWindow::GameLog));
    runningPage->setBackBtnVisible(false);
#else
    MainWindow::getInstance()->hide();
#endif
    m_runGameWorker->start();
}

void GameRunner::exited() {
#if not defined(DEBUG_VERSION)
    MainWindow::getInstance()->show();
#endif
}

void GameRunner::errorOccurred(QProcess::ProcessError) {
    AppState::getInstance()->setError(tr("Could not run the game."));
    emit error();
}

void GameRunner::processClosed(int exitCode) {
    if (exitCode == 0) {
        emit finished();
    } else {
        MainWindow::getInstance()->slideToPage(MainWindow::GameLog);
        GameLogPage *runningPage = static_cast<GameLogPage*>(MainWindow::getInstance()->getPage(MainWindow::GameLog));
        runningPage->setBackBtnVisible(true);
        MainWindow::getInstance()->show();
    }
}
