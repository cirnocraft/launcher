#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonDocument>
#include "struct/appstate.h"
#include "struct/config.h"
#include "widgets/pages/loginpage.h"
#include "states/login.h"
#include "widgets/mainwindow.h"

Login::Login(QObject *parent) : QObject(parent) {
    LoginPage *loginPage = static_cast<LoginPage *>(MainWindow::getInstance()->getPage(MainWindow::Login));
    connect(loginPage, SIGNAL(signIn()), this, SLOT(signIn()));
}

void Login::entered() {
    AppState::getInstance()->setReturnState(AppState::Login);
    if (AppState::getInstance()->getConfig()->getAccessToken().isEmpty()) {
        LoginPage *loginPage = static_cast<LoginPage *>(MainWindow::getInstance()->getPage(MainWindow::Login));
        loginPage->setUsername(AppState::getInstance()->getConfig()->getUsername());
        loginPage->clearPassword();
        MainWindow::getInstance()->slideToPage(MainWindow::Login);
    } else {
        MainWindow::getInstance()->goToPage(MainWindow::Loading);
        validate();
    }
}

void Login::exited() {

}

void Login::validate() {
    MainWindow::getInstance()->showLoading(tr("Signing in..."));

    QNetworkRequest request(QUrl(VALIDATE_URL));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject data;
    data["access_token"] = AppState::getInstance()->getConfig()->getAccessToken();
    QJsonDocument doc(data);
    m_reply = m_networkManager.post(request, doc.toJson());
    QObject::connect(m_reply, SIGNAL(finished()), this, SLOT(signInSuccess()));
    QObject::connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
                     this, SLOT(validateError(QNetworkReply::NetworkError)));
}

void Login::validateError(QNetworkReply::NetworkError) {
    MainWindow::getInstance()->slideToPage(MainWindow::Login);
}

void Login::signIn() {
    MainWindow::getInstance()->showLoading(tr("Signing in..."));

    LoginPage *loginPage = static_cast<LoginPage *>(MainWindow::getInstance()->getPage(MainWindow::Login));
    AppState::getInstance()->getConfig()->setRemember(loginPage->getRemember());

    QNetworkRequest request(QUrl(AUTHENTICATE_URL));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject data = loginPage->getData();
    QJsonDocument doc(data);
    m_reply = m_networkManager.post(request, doc.toJson());
    QObject::connect(m_reply, SIGNAL(finished()), this, SLOT(signInSuccess()));
    QObject::connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
                     this, SLOT(signInError(QNetworkReply::NetworkError)));
}

void Login::signInSuccess() {
    Config *config = AppState::getInstance()->getConfig();
    QJsonDocument doc = QJsonDocument::fromJson(m_reply->readAll());
    QJsonObject data = doc.object();
    config->setUsername(data["username"].toString());
    config->setUserId(data["id"].toString());
    config->setUsername(data["username"].toString());
    config->setAccessToken(data["access_token"].toString());
    emit finished();
}

void Login::signInError(QNetworkReply::NetworkError err) {
    if (err == QNetworkReply::ProtocolInvalidOperationError) {
        AppState::getInstance()->setError(tr("Username and/or password are incorrect. Please, try again."));
    } else {
        AppState::getInstance()->setError(tr("An error has occurred while logging in. "
                                             "Please, check your internet connection and try later."));
    }
    emit error();
}
