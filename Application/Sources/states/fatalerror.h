#ifndef FATALERROR_H
#define FATALERROR_H

#include <QObject>

class FatalError : public QObject {
Q_OBJECT
public:
    explicit FatalError(QObject *parent = 0);
    ~FatalError();

signals:

public slots:
    void entered(void);
    void exited(void);
};

#endif // FATALERROR_H
