#ifndef RECOVERABLEERROR_H
#define RECOVERABLEERROR_H

#include <QObject>
#include <QState>

class RecoverableError : public QObject {
Q_OBJECT
public:
    explicit RecoverableError(QState *loginState, QState *serverChooserState, QObject *parent = 0);
    ~RecoverableError();

signals:
    void toServerChooser(void);
    void toLogin(void);

public slots:
    void entered(void);
    void exited(void);

protected:
    QState *m_loginState, *m_serverChooserState;

protected slots:
    void returnBack();
};

#endif // RECOVERABLEERROR_H
