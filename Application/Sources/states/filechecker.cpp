#include <QThread>
#include "states/filechecker.h"
#include "workers/filecheckworker.h"
#include "struct/appstate.h"

FileChecker::FileChecker(QObject *parent) : QObject(parent) {}

FileChecker::~FileChecker() {

}

void FileChecker::entered() {
    MainWindow::getInstance()->showLoading(tr("Checking files..."));

    QThread *thread = new QThread(this);
    FileCheckWorker *checker = new FileCheckWorker();
    checker->moveToThread(thread);
    QObject::connect(thread, SIGNAL(started()), checker, SLOT(process()));
    QObject::connect(checker, SIGNAL (finished()), thread, SLOT (quit()));
    QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    QObject::connect(checker, SIGNAL(finished()), checker, SLOT(deleteLater()));
    QObject::connect(checker, SIGNAL(checkFinished()), this, SLOT(checkFinished()));
    thread->start();
}

void FileChecker::exited() {

}

void FileChecker::checkFinished() {
    if (AppState::getInstance()->getMissingFilesCount()){
        emit downloadNeeded();
    } else {
        emit finished();
    }
}
