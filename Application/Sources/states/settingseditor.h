#ifndef SETTINGSEDITOR_H
#define SETTINGSEDITOR_H

#include <QObject>
#include <QString>
#include <QStringList>

class SettingsEditor : public QObject {
Q_OBJECT
public:
    explicit SettingsEditor(QObject *parent = 0);
    ~SettingsEditor();

signals:
    void finished(void);
    void logout(void);

public slots:
    void entered(void);
    void exited(void);

protected:
    const QStringList m_xmxLabels = {"1GB", "1.5GB", "2GB"};
    const QStringList m_xmxValues = {"1024", "1536", "2048"};

protected slots:
    void doLogout();
    void xmxChanged(int);
};

#endif // SETTINGSEDITOR_H
