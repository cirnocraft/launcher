#ifndef GAMERUNNER_H
#define GAMERUNNER_H

#include <QObject>
#include <QString>
#include <QProcess>
#include "workers/rungameworker.h"

class GameRunner : public QObject {
Q_OBJECT
public:
    explicit GameRunner(QObject *parent = 0);
    ~GameRunner();

signals:
    void finished(void);
    void error(void);
    void showLog(void);

public slots:
    void entered(void);
    void exited(void);

protected:
    RunGameWorker *m_runGameWorker;

protected slots:
    void errorOccurred(QProcess::ProcessError);
    void processClosed(int);
};

#endif // GAMERUNNER_H
