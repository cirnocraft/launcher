#include <QThread>
#include <QtMath>
#include "states/fileretriever.h"
#include "widgets/mainwindow.h"
#include "workers/downloadworker.h"
#include "struct/appstate.h"

FileRetriever::FileRetriever(QObject *parent) : QObject(parent) {
    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(timer()));
}

FileRetriever::~FileRetriever() {

}

void FileRetriever::entered() {
    MainWindow::getInstance()->showLoading(tr("Downloading..."));
    m_speedMeasures.clear();
    m_speedMeasures.push_back(0);

    QThread *thread = new QThread(this);
    DownloadWorker *downloader = new DownloadWorker();
    QObject::connect(thread, SIGNAL(started()), downloader, SLOT(process()));
    QObject::connect(downloader, SIGNAL (finished()), thread, SLOT (quit()));
    QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    QObject::connect(downloader, SIGNAL(finished()), downloader, SLOT(deleteLater()));
    QObject::connect(downloader, SIGNAL(progress(int, int)),
                     this, SLOT(downloadProgress(int, int)));
    QObject::connect(downloader, SIGNAL(updateByteCount(qint64)),
                     this, SLOT(updateByteCount(qint64)));
    QObject::connect(downloader, SIGNAL(success()),
                     this, SIGNAL(finished()));
    QObject::connect(downloader, SIGNAL(error(QString)),
                     this, SLOT(downloadError(QString)));
    thread->start();
    m_timer.start(1000);
}

void FileRetriever::exited() {
    m_timer.stop();
    MainWindow::getInstance()->showLoading(" ", " ", " ");

}

void FileRetriever::updateMessage() {
    MainWindow::getInstance()->showLoading(tr("Downloading..."), line2, line3);
}

void FileRetriever::downloadProgress(int ready, int total) {
    line2 = tr("%1/%2 ready").arg(ready).arg(total);
    updateMessage();
}

void FileRetriever::downloadError(QString message) {
    AppState::getInstance()->setError(message);
    emit error();
}

void FileRetriever::updateByteCount(qint64 count) {
    m_speedMeasures[0] += count;
}

void FileRetriever::timer() {
    qint64 speed = 0;
    int samples = qMin(m_speedMeasures.count(), 8);
    for (int i = 0; i < samples; i++) {
        speed += m_speedMeasures[i];
    }
    line3 = tr("%1 kbps").arg(speed / 1024 / samples);
    updateMessage();
    m_speedMeasures.push_front(0);
}
