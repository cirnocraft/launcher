#ifndef FILECHECKER_H
#define FILECHECKER_H

#include <QObject>
#include <QString>
#include <QList>
#include "struct/manifestfile.h"

class FileChecker : public QObject {
Q_OBJECT
public:
    explicit FileChecker(QObject *parent = 0);
    ~FileChecker();

signals:
    void downloadNeeded();
    void finished();

public slots:
    void entered(void);
    void exited(void);

protected slots:
    void checkFinished();
};

#endif // FILECHECKER_H
