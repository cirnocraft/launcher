#include "widgets/mainwindow.h"
#include "widgets/pages/errorpage.h"
#include "states/recoverableerror.h"
#include "struct/appstate.h"

RecoverableError::RecoverableError(QState *loginState, QState *serverChooserState, QObject *parent) : QObject(parent) {
    m_loginState = loginState;
    m_serverChooserState = serverChooserState;
    ErrorPage *errorPage = static_cast<ErrorPage *>(MainWindow::getInstance()->getPage(MainWindow::Error));
    QObject::connect(errorPage, SIGNAL(returnBack()), this, SLOT(returnBack()));
}

RecoverableError::~RecoverableError() {

}

void RecoverableError::entered() {
    MainWindow::getInstance()->slideToPage(MainWindow::Error);
}

void RecoverableError::exited() {

}

void RecoverableError::returnBack() {
    switch (AppState::getInstance()->getReturnState()) {
    case AppState::Login:
        emit toLogin();
        break;
    case AppState::ServerChooser:
        emit toServerChooser();
        break;
    }
}
