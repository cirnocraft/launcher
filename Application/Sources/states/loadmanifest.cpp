#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonArray>
#include "states/loadmanifest.h"
#include "struct/gamemanifest.h"
#include "struct/manifestfile.h"
#include "struct/appstate.h"

LoadManifest::LoadManifest(QObject *parent) : QObject(parent) {

}

LoadManifest::~LoadManifest() {

}

void LoadManifest::entered() {
    qDebug() << "Downloading manifests";
    MainWindow::getInstance()->showLoading(tr("Loading..."));
    QNetworkRequest request(QUrl(MANIFESTS_URL));
    m_reply = m_networkManager.get(request);
    QObject::connect(m_reply, SIGNAL(finished()),
                     this, SLOT(downloadFinished()));
    QObject::connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
                     this, SLOT(downloadError(QNetworkReply::NetworkError)));
}

void LoadManifest::exited() {
    delete m_reply;
}

void LoadManifest::downloadError(QNetworkReply::NetworkError) {
    AppState::getInstance()->setError(tr("Couldn't download manifest"));
    emit error();
}

void LoadManifest::downloadFinished() {
    qDebug() << "Parsing manifests";
    QJsonDocument json;
    try {
        json = QJsonDocument::fromJson(m_reply->readAll());
    } catch (QJsonParseError) {
        AppState::getInstance()->setError(tr("Can't parse manifest json"));
        emit error();
        return;
    }
    QJsonObject manifests = json.object();
    for (QJsonObject::iterator it = manifests.begin(); it != manifests.end(); ++it) {
        GameManifest *manifest = new GameManifest();
        QJsonObject manifest_obj = it.value().toObject();
        manifest->id = it.key();
        manifest->name = manifest_obj["name"].toString();
        manifest->baseVersion = manifest_obj["version"].toString();
        manifest->mainClass = manifest_obj["main_class"].toString();
        manifest->args = manifest_obj["args"].toString();
        manifest->server = manifest_obj["server"].toString();
        manifest->port = manifest_obj["port"].toString();
        manifest->assetsVersion = manifest_obj["assets"].toString();
        manifest->client = ManifestFile(manifest_obj["client"].toObject(),
                                        manifest, ManifestFile::ClientJar);
        manifest->assets = ManifestFile(manifest_obj["assets_jar"].toObject(),
                                        manifest, ManifestFile::AssetsJar);

        // shared libs
        QJsonArray libraries = manifest_obj["libraries_shared"].toArray();
        for (int i=0; i < libraries.count(); ++i) {
            manifest->libraries.push_back(ManifestFile(libraries[i].toObject(),
                                                       manifest, ManifestFile::LibraryJar));
        }

        // platform-dependent
#if defined(Q_OS_WIN)
        libraries = manifest_obj["libraries_windows"].toArray();
#elif defined(Q_OS_LINUX)
        libraries = manifest_obj["libraries_linux"].toArray();
#elif defined(Q_OS_MAC)
        libraries = manifest_obj["libraries_osx"].toArray();
#endif
        for (int i=0; i < libraries.count(); ++i) {
            manifest->libraries.push_back(ManifestFile(libraries[i].toObject(),
                                                       manifest, ManifestFile::LibraryJar));
        }

        // native libs checksums
#if defined(Q_OS_WIN)
        libraries = manifest_obj["natives_windows"].toArray();
#elif defined(Q_OS_LINUX)
        libraries = manifest_obj["natives_linux"].toArray();
#elif defined(Q_OS_MAC)
        libraries = manifest_obj["natives_osx"].toArray();
#endif
        for (int i=0; i < libraries.count(); ++i) {
            manifest->natives.push_back(ManifestFile(libraries[i].toObject(),
                                                     manifest, ManifestFile::LibraryJar));
        }

        AppState::getInstance()->addManifest(manifest);
    }
    emit finished();
    qDebug() << "Manifests ready";
}
