#ifndef SERVERCHOOSER_H
#define SERVERCHOOSER_H

#include <QObject>

class ServerChooser : public QObject {
Q_OBJECT
public:
    explicit ServerChooser(QObject *parent = 0);
    ~ServerChooser();

signals:
    void runGame(void);
    void openSettings(void);

public slots:
    void entered(void);
    void exited(void);
};

#endif // SERVERCHOOSER_H
