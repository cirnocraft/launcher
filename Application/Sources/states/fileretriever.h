#ifndef FILERETRIEVER_H
#define FILERETRIEVER_H

#include <QObject>
#include <QString>
#include <QTimer>
#include <QList>

class FileRetriever : public QObject {
Q_OBJECT
public:
    explicit FileRetriever(QObject *parent = 0);
    ~FileRetriever();

signals:
    void finished(void);
    void error(void);

public slots:
    void entered(void);
    void exited(void);

protected:
    void updateMessage();

    QString line2, line3;
    QTimer m_timer;
    QList<qint64> m_speedMeasures;

protected slots:
    void downloadProgress(int, int);
    void downloadError(QString);
    void updateByteCount(qint64);
    void timer();
};

#endif // FILERETRIEVER_H
