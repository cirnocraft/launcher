#ifndef INIT_H
#define INIT_H

#include <QObject>
#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class LoadManifest : public QObject {
Q_OBJECT
public:
    explicit LoadManifest(QObject *parent = 0);
    ~LoadManifest();

signals:
    void finished(void);
    void error(void);

public slots:
    void entered(void);
    void exited(void);

protected:
    QNetworkAccessManager m_networkManager;
    QNetworkReply *m_reply;

protected slots:
    void downloadFinished();
    void downloadError(QNetworkReply::NetworkError);
};

#endif // INIT_H
