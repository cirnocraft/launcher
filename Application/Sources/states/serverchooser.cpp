#include <QStringList>
#include <QDebug>
#include "states/serverchooser.h"
#include "struct/appstate.h"
#include "widgets/mainwindow.h"
#include "widgets/pages/playpage.h"

ServerChooser::ServerChooser(QObject *parent) : QObject(parent) {
    ServerChooserPage *serverChoosePage = static_cast<ServerChooserPage *>(MainWindow::getInstance()->getPage(MainWindow::ServerChooser));
    QObject::connect(serverChoosePage, SIGNAL(playClicked()),
                     this, SIGNAL(runGame()));
    QObject::connect(serverChoosePage, SIGNAL(settingsClicked()),
                     this, SIGNAL(openSettings()));
}

ServerChooser::~ServerChooser() {

}

void ServerChooser::entered() {
    QStringList servers;
    for (int i = 0; i < AppState::getInstance()->getManifestsCount(); i++) {
        servers.append(AppState::getInstance()->getManifest(i)->name);
    }
    static_cast<ServerChooserPage *>(MainWindow::getInstance()->getPage(MainWindow::ServerChooser))
            ->initServersList(servers);
    MainWindow::getInstance()->slideToPage(MainWindow::ServerChooser);
}

void ServerChooser::exited() {

}
