#ifndef LOGIN_H
#define LOGIN_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class Login : public QObject {
Q_OBJECT
public:
    explicit Login(QObject *parent = 0);

signals:
    void finished();
    void error();

public slots:
    void entered(void);
    void exited(void);

protected:
    QNetworkAccessManager m_networkManager;
    QNetworkReply *m_reply;

protected slots:
    void validate();
    void validateError(QNetworkReply::NetworkError);
    void signIn();
    void signInSuccess();
    void signInError(QNetworkReply::NetworkError);
};

#endif // LOGIN_H
