#ifndef DOWNLOADWORKER_H
#define DOWNLOADWORKER_H

#include <QObject>
#include <QString>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFile>
#include "struct/gamemanifest.h"
#include "struct/manifestfile.h"

class FileDownloadWorker : public QObject {
Q_OBJECT
public:
    explicit FileDownloadWorker(ManifestFile *file, QNetworkAccessManager *networkManager, QObject *parent = 0);
    ~FileDownloadWorker();

    enum Error { IOError, NetError, HashError, ExtractError };

signals:
    void finished(ManifestFile*);
    void error(ManifestFile*, FileDownloadWorker::Error);
    void updateByteCount(qint64);

protected:
    ManifestFile *m_manifestFile;
    QNetworkRequest m_request;
    QNetworkReply *m_reply;
    QFile *m_file;
    qint64 m_byteCount = 0;

protected slots:
    void downloadStep(qint64 ready, qint64 total);
    void downloadFinished(void);
    void downloadError(QNetworkReply::NetworkError err);
};

class DownloadWorker : public QObject {
Q_OBJECT
public:
    explicit DownloadWorker(QObject *parent = 0);
    ~DownloadWorker();

    void downloadFile(ManifestFile *file);

signals:
    void finished();
    void progress(int ready, int total);
    void success();
    void error(QString message);
    void updateByteCount(qint64);

public slots:
    void process(void);
    void extract(ManifestFile *file);
    void fileDownloadFinished(ManifestFile *file);
    void fileDownloadError(ManifestFile *file, FileDownloadWorker::Error err);

protected:
    QList<FileDownloadWorker*> m_downloaders;
    QNetworkAccessManager m_networkManager;
    int m_downloaded = 0;
};

#endif // DOWNLOADWORKER_H
