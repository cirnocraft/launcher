#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include <quazip.h>
#include <quazipfile.h>
#include "workers/downloadworker.h"
#include "workers/filecheckworker.h"
#include "struct/appstate.h"
#include "pathhelper.h"

// TODO: freeze timeouts

DownloadWorker::DownloadWorker(QObject *parent) : QObject(parent) {}

DownloadWorker::~DownloadWorker() {}

void DownloadWorker::downloadFile(ManifestFile *file) {
    FileDownloadWorker *fileDownloader = new FileDownloadWorker(file, &m_networkManager);
    QObject::connect(fileDownloader, SIGNAL(error(ManifestFile*, FileDownloadWorker::Error)),
                     this, SLOT(fileDownloadError(ManifestFile*, FileDownloadWorker::Error)));
    QObject::connect(fileDownloader, SIGNAL(updateByteCount(qint64)),
                     this, SIGNAL(updateByteCount(qint64)));
    if (file->extract) {
        QObject::connect(fileDownloader, SIGNAL(finished(ManifestFile*)),
                         this, SLOT(extract(ManifestFile*)));
    } else {
        QObject::connect(fileDownloader, SIGNAL(finished(ManifestFile*)),
                         this, SLOT(fileDownloadFinished(ManifestFile*)));
    }
    m_downloaders.push_back(fileDownloader);
}

void DownloadWorker::process() {
    for (int i = 0; i < AppState::getInstance()->getMissingFilesCount(); i++) {
        downloadFile(AppState::getInstance()->getMissingFile(i));
    }
}

void DownloadWorker::extract(ManifestFile *file) {
    QDir dir = PathHelper::nativesDir(AppState::getInstance()->getCurrentManifest()->id);
    dir.mkpath(dir.absolutePath());
    QuaZip zip(file->localPath);
    if (!zip.open(QuaZip::mdUnzip)) {
        fileDownloadError(file, FileDownloadWorker::ExtractError);
        return;
    }
    for (bool f = zip.goToFirstFile(); f; f = zip.goToNextFile()) {
        QString fileName = zip.getCurrentFileName();
        QuaZipFile packedFile(zip.getZipName(), fileName);
        if (!packedFile.open(QFile::ReadOnly)) {
            zip.close();
            fileDownloadError(file, FileDownloadWorker::ExtractError);
            return;
        }
        if (fileName.startsWith("META_INF"))
            continue;
        QFile outFile(PathHelper::nativePath(AppState::getInstance()->getCurrentManifest()->id, fileName));
        if (!outFile.open(QFile::WriteOnly)) {
            packedFile.close();
            zip.close();
            fileDownloadError(file, FileDownloadWorker::ExtractError);
            return;
        }
        outFile.write(packedFile.readAll());
        outFile.close();
        packedFile.close();
    }
    zip.close();
    fileDownloadFinished(file);
}

void DownloadWorker::fileDownloadFinished(ManifestFile *file) {
    if (!FileCheckWorker::checkFile(file->localPath, file->sha1)) {
        fileDownloadError(file, FileDownloadWorker::HashError);
    } else {
        if (++m_downloaded == m_downloaders.count()) {
            qDebug() << "Download finished";
            emit finished();
            emit success();
        } else {
            emit progress(m_downloaded, m_downloaders.count());
        }
    }
}

void DownloadWorker::fileDownloadError(ManifestFile *file, FileDownloadWorker::Error err) {
    switch (err) {
    case FileDownloadWorker::NetError:
        emit error(tr("Unable to download file %1").arg(file->url));
        break;
    case FileDownloadWorker::IOError:
        emit error(tr("Unable to write file %1").arg(file->localPath));
        break;
    case FileDownloadWorker::HashError:
        emit error(tr("Some of downloaded files have wrong checksum"));
        break;
    case FileDownloadWorker::ExtractError:
        emit error(tr("Unable to extract some archives "));
        break;
    }
    emit finished();
}

FileDownloadWorker::FileDownloadWorker(ManifestFile *manifestFile, QNetworkAccessManager *networkManager, QObject *parent) : QObject(parent) {
    m_manifestFile = manifestFile;
    QDir dir = QFileInfo(m_manifestFile->localPath).dir();
    dir.mkpath(dir.absolutePath());
    m_file = new QFile(m_manifestFile->localPath);

    if (m_file->open(QFile::WriteOnly)) {
        m_request = QNetworkRequest(QUrl(m_manifestFile->url));
        m_reply = networkManager->get(m_request);
        QObject::connect(m_reply, SIGNAL(downloadProgress(qint64,qint64)),
                         this, SLOT(downloadStep(qint64,qint64)));
        QObject::connect(m_reply, SIGNAL(finished()),
                         this, SLOT(downloadFinished()));
        QObject::connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
                         this, SLOT(downloadError(QNetworkReply::NetworkError)));
    } else {
        emit error(m_manifestFile, FileDownloadWorker::IOError);
    }
}

FileDownloadWorker::~FileDownloadWorker() {}

void FileDownloadWorker::downloadStep(qint64 count, qint64) {
    emit updateByteCount(count - m_byteCount);
    m_byteCount = count;
    m_file->write(m_reply->readAll());
}

void FileDownloadWorker::downloadFinished() {
    m_file->close();
    emit finished(m_manifestFile);
    deleteLater();
}

void FileDownloadWorker::downloadError(QNetworkReply::NetworkError) {
    m_file->close();
    emit error(m_manifestFile, NetError);
    deleteLater();
}
