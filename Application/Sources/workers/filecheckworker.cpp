#include <QFile>
#include <QCryptographicHash>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include "workers/filecheckworker.h"
#include "struct/appstate.h"
#include "pathhelper.h"

FileCheckWorker::FileCheckWorker(QObject *parent) : QObject(parent) {}

FileCheckWorker::~FileCheckWorker() {}

void FileCheckWorker::process() {
    GameManifest *manifest = AppState::getInstance()->getCurrentManifest();
    qDebug() << "Checking" << manifest->id << "files";
    if (!checkFile(manifest->client.localPath, manifest->client.sha1))
        AppState::getInstance()->addMissingFile(&manifest->client);

    if (!checkFile(manifest->assets.localPath, manifest->assets.sha1))
        AppState::getInstance()->addMissingFile(&manifest->assets);

    for (int i = 0; i < manifest->libraries.count(); i++) {
        if (!checkFile(manifest->libraries[i].localPath, manifest->libraries[i].sha1))
            AppState::getInstance()->addMissingFile(&manifest->libraries[i]);
    }

    for (int i = 0; i < manifest->natives.count(); i++) {
        if (!checkFile(manifest->natives[i].localPath, manifest->natives[i].sha1)) {
            AppState::getInstance()->addMissingFile(&manifest->natives[i]);
        } else {
            for (int j = 0; j < manifest->natives[i].files.count(); j++) {
                if (!checkFile(manifest->natives[i].files[j].localPath, manifest->natives[i].files[j].sha1)) {
                    AppState::getInstance()->addMissingFile(&manifest->natives[i]);
                    break;
                }
            }
        }
    }

    qDebug() << "Done," << AppState::getInstance()->getMissingFilesCount() << "file(s) to download";
    emit checkFinished();
    emit finished();
}

bool FileCheckWorker::checkFile(QString path, QString sha1) {
    QFile f(path);
    QCryptographicHash hash(QCryptographicHash::Algorithm::Sha1);
    return f.open(QFile::ReadOnly) && hash.addData(&f) && hash.result().toHex() == sha1;
}
