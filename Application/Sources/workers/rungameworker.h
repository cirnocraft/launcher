#ifndef RUNGAMEWORKER_H
#define RUNGAMEWORKER_H

#include <QObject>
#include <QString>
#include <QProcess>
#include "struct/gamemanifest.h"

class RunGameWorker : public QObject {
Q_OBJECT
public:
    explicit RunGameWorker(QObject *parent = 0);
    ~RunGameWorker();

    void start();

signals:
    void errorOccurred(QProcess::ProcessError);
    void finished(int);

protected:
    QProcess *m_gameProcess;

    QString getClassPath();
    QStringList getArgs();

protected slots:
    void readStdout(void);
    void readStderr(void);
};

#endif // RUNGAMEWORKER_H
