#include <QDebug>
#include "workers/rungameworker.h"
#include "struct/appstate.h"
#include "pathhelper.h"

RunGameWorker::RunGameWorker(QObject *parent) : QObject(parent) {
    m_gameProcess = new QProcess;
}

RunGameWorker::~RunGameWorker() {
}

void RunGameWorker::start() {
    QObject::connect(m_gameProcess, SIGNAL(finished(int)),
                     this, SIGNAL(finished(int)));
    QObject::connect(m_gameProcess, SIGNAL(errorOccurred(QProcess::ProcessError)),
                     this, SIGNAL(errorOccurred(QProcess::ProcessError)));
    QObject::connect(m_gameProcess, SIGNAL(readyReadStandardOutput()),
                     this, SLOT(readStdout()));
    QObject::connect(m_gameProcess, SIGNAL(readyReadStandardError()),
                     this, SLOT(readStderr()));

    QStringList args;
    args << "-Djava.library.path=" + PathHelper::nativesDir(AppState::getInstance()->getCurrentManifest()->id)
         << "-Xms512m"
         << "-Xmx" + AppState::getInstance()->getConfig()->getXmx() + "m"
         << "-XX:-UseAdaptiveSizePolicy"
         << "-XX:+UseG1GC"
         << "-XX:InitiatingHeapOccupancyPercent=60"
         << "-XX:MaxGCPauseMillis=200"
         << "-client"
         << "-cp" << getClassPath()
         << AppState::getInstance()->getCurrentManifest()->mainClass
         << "--server" << AppState::getInstance()->getCurrentManifest()->server
         << "--port" << AppState::getInstance()->getCurrentManifest()->port;
    args += getArgs();
    // qDebug() << args.join(" ");
    m_gameProcess->setWorkingDirectory(PathHelper::versionDir(AppState::getInstance()->getCurrentManifest()->id));
    m_gameProcess->start("java", args);
}

QString RunGameWorker::getClassPath() {
    QStringList classPath;
    for (int i = 0; i < AppState::getInstance()->getCurrentManifest()->libraries.count(); i++) {
        classPath << AppState::getInstance()->getCurrentManifest()->libraries[i].localPath;
    }
    classPath << AppState::getInstance()->getCurrentManifest()->client.localPath
              << AppState::getInstance()->getCurrentManifest()->assets.localPath;
#if defined(Q_OS_WIN)
    QString sep = ";";
#else
    QString sep = ":";
#endif
    return classPath.join(sep);
}

QStringList RunGameWorker::getArgs() {
    return AppState::getInstance()->getCurrentManifest()->args
            .replace("{auth_player_name}", "bakatrouble")
            .replace("{version_name}", AppState::getInstance()->getCurrentManifest()->baseVersion)
            .replace("{game_directory}", PathHelper::versionDir(AppState::getInstance()->getCurrentManifest()->id))
            .replace("{assets_root}", PathHelper::assetsDir())
            .replace("{auth_uuid}", AppState::getInstance()->getConfig()->getUserId())
            .replace("{auth_access_token}", AppState::getInstance()->getConfig()->getAccessToken())
            .replace("{user_type}", "mojang")
            .replace("{version_type}", "release")
            .split(" ");
}

void RunGameWorker::readStdout() {
    AppState::getInstance()->appendLog(m_gameProcess->readAllStandardOutput());
}

void RunGameWorker::readStderr() {
    AppState::getInstance()->appendLog(m_gameProcess->readAllStandardError());
}
