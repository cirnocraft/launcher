#ifndef FILECHECKWORKER_H
#define FILECHECKWORKER_H

#include <QObject>
#include <QString>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include "struct/manifestfile.h"
#include "struct/gamemanifest.h"

class FileCheckWorker : public QObject {
Q_OBJECT
public:
    explicit FileCheckWorker(QObject *parent = 0);
    ~FileCheckWorker();

    static bool checkFile(QString path, QString sha1);

signals:
    void checkFinished();
    void finished();

public slots:
    void process(void);
};

#endif // FILECHECKWORKER_H
