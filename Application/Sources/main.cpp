#include <QApplication>
#include <QTimer>
#include <QDebug>
#include <QStateMachine>
#include <QState>
#include <QStyleFactory>
#include <QDebug>
#include "struct/style.h"
#include "widgets/mainwindow.h"
#include "struct/appstate.h"
#include "app.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    QApplication::setStyle(new Style("plastique"));

    new AppState();

    MainWindow w;
    w.show();

    App app;
    app.start();

    return a.exec();
}
