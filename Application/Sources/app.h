#ifndef APP_H
#define APP_H

#include <QObject>
#include <QStateMachine>
#include <QState>
#include <QFinalState>
#include <QList>

#include "struct/gamemanifest.h"
#include "states/login.h"
#include "states/loadmanifest.h"
#include "states/serverchooser.h"
#include "states/settingseditor.h"
#include "states/filechecker.h"
#include "states/fileretriever.h"
#include "states/gamerunner.h"
#include "states/fatalerror.h"
#include "states/recoverableerror.h"

class App : public QObject {
Q_OBJECT
public:
    explicit App(QObject *parent = 0);
    ~App();

    void start();

protected:
    QStateMachine *m_stateMachine;

    Login *m_login;
    QState *m_loginState;

    LoadManifest *m_loadManifest;
    QState *m_loadManifestState;

    ServerChooser *m_serverChooser;
    QState *m_serverChooserState;

    SettingsEditor *m_settingsEditor;
    QState *m_settingsEditorState;

    FileChecker *m_fileChecker;
    QState *m_fileCheckerState;

    FileRetriever *m_fileRetriever;
    QState *m_fileRetrieverState;

    GameRunner *m_gameRunner;
    QState *m_gameRunnerState;

    RecoverableError *m_recoverableError;
    QState *m_recoverableErrorState;
};

#endif // APP_H
