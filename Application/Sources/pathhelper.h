#ifndef PATHHELPER_H
#define PATHHELPER_H

#include <QPair>
#include <QString>
#include <QStringList>
#include "struct/gamemanifest.h"

class PathHelper {
public:
    static QString baseDir();

    static QString librariesDir();
    static QString assetsDir();
    static QString versionsDir();
    static QString versionDir(QString id);
    static QString nativesDir(QString id);

    static QString libraryPath(QString name);
    static QString assetsPath(QString version);
    static QString versionPath(QString id);
    static QString nativePath(QString id, QString name);

    static QStringList libraryPackage(QString name);
    static QPair<QString, QString> libraryInfo(QString name);
};

#endif // PATHHELPER_H
