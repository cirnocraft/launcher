#include <QStandardPaths>
#include <QDir>
#include <QPair>
#include "pathhelper.h"

QString PathHelper::baseDir() {
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString PathHelper::librariesDir() {
    return QDir(PathHelper::baseDir()).filePath("libraries");
}

QString PathHelper::assetsDir() {
    return QDir(PathHelper::baseDir()).filePath("assets");
}

QString PathHelper::versionsDir() {
    return QDir(PathHelper::baseDir()).filePath("versions");
}

QString PathHelper::versionDir(QString id) {
    return QDir(PathHelper::versionsDir()).filePath(id);
}

QString PathHelper::nativesDir(QString id) {
    return QDir(PathHelper::versionDir(id)).filePath("natives");
}

QString PathHelper::libraryPath(QString name) {
    QPair<QString, QString> libraryInfo = PathHelper::libraryInfo(name);
    QStringList libraryPackage = PathHelper::libraryPackage(name);
    QString path = QStringLiteral("%1/%2/%3/%2-%3.jar").arg(libraryPackage.join("/"), libraryInfo.first, libraryInfo.second);
    return QDir(PathHelper::librariesDir()).filePath(path);
}

QString PathHelper::assetsPath(QString id) {
    return QDir(PathHelper::versionDir(id)).filePath("assets.jar");
}

QString PathHelper::versionPath(QString id) {
    return QDir(PathHelper::versionDir(id)).filePath(QStringLiteral("%1.jar").arg(id));
}

QString PathHelper::nativePath(QString id, QString name) {
    return QDir(PathHelper::nativesDir(id)).filePath(name);
}

QStringList PathHelper::libraryPackage(QString name) {
    QStringList libraryInfo = name.split(":");
    QStringList libraryPackage = libraryInfo[0].split(".");
    return libraryPackage;
}

QPair<QString, QString> PathHelper::libraryInfo(QString name) {
    QStringList libraryInfo = name.split(":");
    return QPair<QString, QString>(libraryInfo[1], libraryInfo[2]);
}
